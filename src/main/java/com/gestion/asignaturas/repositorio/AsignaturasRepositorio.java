package com.gestion.asignaturas.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gestion.asignaturas.modelo.Asignatura;


public interface AsignaturasRepositorio extends JpaRepository<Asignatura, Integer> {

}
