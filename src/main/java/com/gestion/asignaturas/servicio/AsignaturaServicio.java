package com.gestion.asignaturas.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gestion.asignaturas.modelo.Asignatura;
import com.gestion.asignaturas.repositorio.AsignaturasRepositorio;

import org.springframework.stereotype.Service;

@Service

public class AsignaturaServicio {
	
	@Autowired
	
	private AsignaturasRepositorio repositorio;
	
	public List <Asignatura> listarAsignaturas(){
		return repositorio.findAll();
	}

	
	public void guardarAsignatura(Asignatura asignatura) {
		repositorio.save(asignatura);
	}
	
	public Asignatura obtenerAsignaturaPorId (Integer id) {
		return repositorio.findById(id).get();
	}
	
	public void eliminarProducto(Integer id) {
		repositorio.deleteById(id);
	}
}
