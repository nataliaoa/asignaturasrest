package com.gestion.asignaturas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class AsignaturasRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsignaturasRestApplication.class, args);
	}

}