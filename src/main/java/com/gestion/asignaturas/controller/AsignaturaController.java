package com.gestion.asignaturas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gestion.asignaturas.modelo.Asignatura;
import com.gestion.asignaturas.servicio.AsignaturaServicio;

@RestController

public class AsignaturaController {
	
	@Autowired
	private AsignaturaServicio servicio;
	
	@GetMapping("/asignaturas")
	public List<Asignatura> listarAsignaturas(){
		return servicio.listarAsignaturas();
	}
	
	
	@GetMapping("/asignaturas/{id}")
	public ResponseEntity<Asignatura> obtenerAsignatura(@PathVariable Integer id) {
		try {
			Asignatura asignatura= servicio.obtenerAsignaturaPorId(id);
			return new ResponseEntity<Asignatura>(asignatura,HttpStatus.OK);
		}
		catch (Exception excepcion){
			return new ResponseEntity<Asignatura>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/asignaturas")
	public void guardarAsignatura (@RequestBody Asignatura asignatura) {
		servicio.guardarAsignatura(asignatura);
	}
	
	@PutMapping("/asignaturas/{id}")
	public ResponseEntity<?> actualizarAsignatura (@RequestBody Asignatura asignatura, @PathVariable Integer id) {
		try {
			Asignatura asignaturaExistente =servicio.obtenerAsignaturaPorId(id);
			asignaturaExistente.setNombre(asignatura.getNombre());
		
			servicio.guardarAsignatura(asignaturaExistente);
			return new ResponseEntity<Asignatura>(HttpStatus.OK); 
		}
		catch (Exception excepcion) {
			return new ResponseEntity<Asignatura>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping ("/asignaturas/{id}")
	public void eliminarAsignatura(@PathVariable Integer id) {
		servicio.eliminarProducto(id);
	}
	
}
